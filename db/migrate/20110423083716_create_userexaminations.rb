class CreateUserexaminations < ActiveRecord::Migration
  def self.up
    create_table :userexaminations do |t|

      t.integer :user_id
      t.integer :examination_id
      t.boolean :exam_active_status, :default =>0
      t.integer :number_of_attempts, :default =>0
      t.boolean :exam_complete_status, :default =>0
      t.integer :duration
      t.integer :questions_answered
      t.integer :time_remain
      
      t.timestamps
    end
  end

  def self.down
    drop_table :userexaminations
  end
end
