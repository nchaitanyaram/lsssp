class QuestionbanksController < ApplicationController
  layout "admin"
  before_filter :require_user, :only => [:index,:show, :edit, :update]

  def index
    @questionbanks = Questionbank.all
    @subjects = Subject.all
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @questionbanks }
    end
  end

  def show
    @questionbank = Questionbank.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @questionbank }
    end
  end

  def new
  	@module_id = params[:module_id]
  	@module_name = Subject.find(params[:module_id]).name
    @questionbank = Questionbank.new
    @questionbanks = Questionbank.find(:all, :conditions=>{:subject_id => @module_id})

    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @questionbank }
    end
  end

  def edit
    @questionbank = Questionbank.find(params[:id])
  end

  def create
    @questionbank = Questionbank.new(params[:questionbank])
    @questionbanks = Questionbank.all
    
    
#    puts 'XXXXXXXXXXXXXXXXXXXXX'
#	puts subjectcode = params[:module_id]
    
    respond_to do |format|
      if @questionbank.save
#      if params[:questionbank][:question_type]=="image"
#        Questionbank.save_images(params[:questionbank])
#      end
        format.html { redirect_to :back }
#        format.xml  { render :xml => @questionbank, :status => :created, :location => @questionbank }
         format.js
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @questionbank.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @questionbank = Questionbank.find(params[:id])

    respond_to do |format|
      if @questionbank.update_attributes(params[:questionbank])
        format.html { redirect_to(@questionbank, :notice => 'Questionbank was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @questionbank.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @questionbank = Questionbank.find(params[:id])
    @questionbank.destroy

    respond_to do |format|
      format.html { redirect_to(questionbanks_url) }
      format.xml  { head :ok }
    end
  end
end
